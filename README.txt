README.txt
----------

*************************************************************************
This is Avline module and works with Drupal 7.x
*************************************************************************

This module displays a block with the users avatars who are online, if
they don't have avatar, the module displays a default image, if the user
is not registered on the page, the module display a default one too, which
can be changed in the folder module.

You can set the block to show only registered users or anonymous, you can
also control the number of users to display, default is set to display a
maximum of 20.

In the permissions pannel you can control the visibility of the block.

Depends on the Contact module for contact with others if you have the
Administrator role.

Module open to anyone who wants to extend it and test it.

*************************************************************************
Ash07.